import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}){

	let { name, description, price, _id } = productProp;

	return(
		<Card className='cardbody'>
			<Card.Body>
				<Card.Title className="text-center">{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Button size="sm" className="cardbutton" as={Link} to={`/products/${_id}`}>View Product</Button>
			</Card.Body>
		</Card>
	)
}
