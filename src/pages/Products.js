import { useEffect, useState, useContext  } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import ProductCard from '../components/ProductCard';

export default function Products(){

	const [products, setProducts] =useState([])

	const {user} = useContext(UserContext);

	useEffect(()=>{
	fetch(`https://caps-3-backend.onrender.com/products/active`)
	.then(res=>res.json())
	.then(data=>{

	const productArr = (data.map(product => {
		return (
					<ProductCard productProp={product} key={product._id}/>
					)
				}))
				setProducts(productArr)
			})

			},[products])


	return(
		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<h1>Products</h1>
			{products}
	
		</>
	)
}
